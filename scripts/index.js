// Створити порожній об'єкт student, з полями name та lastName. +
// Запитати у користувача ім'я та прізвище студента, отримані значення записати у відповідні поля об'єкта. +
// У циклі запитувати у користувача назву предмета та оцінку щодо нього. Якщо користувач натисне Cancel при n-питанні про назву предмета, закінчити цикл. Записати оцінки з усіх предметів як студента tabel.
// порахувати кількість поганих (менше 4) оцінок з предметів. Якщо таких немає, вивести повідомлення "Студент переведено на наступний курс".
// Порахувати середній бал з предметів. Якщо він більше 7 – вивести повідомлення Студенту призначено стипендію.

const name = prompt('Як тебе звати?')
const lastName = prompt('Яке в тебе прізвище?')
let table = {}

const student = {
    name,
    lastName,
    table,
}

do {
    let subject = prompt('який предмет?')
    if (!subject) break;

    let mark = prompt('яка оцінка?');
    student.table[subject] = mark;
} while (true);

let value = 0;
let counter = 0;
let allMarks = 0; 

 for (let key in student.table) {
     if (student.table[key] < 4) {
        value++;
     }
     
     counter++;
     allMarks += +student.table[key]
 }
    
if (value > 0) {
        console.log(`Кількість поганих оцінок: ${value}`)
    }
if (value === 0) {
         console.log(`Студента переведено на наступний курс`)
    }
if (allMarks / counter > 7) {
    console.log('Студенту призначено стипендію')
}